var socketIO = require('socket.io');
var io;

function start(httpServer) {
	io = socketIO.listen(httpServer);
	io.sockets.on('connection', function(socket) {
		// Handle socket incoming events
		socket.on('talk event', function(data) {
			// sending to all clients except sender
			socket.broadcast.emit('new msg event', data);
		});

		// Handle socket disconnection
		socket.on('disconnect', function() {
			console.log("Socket[" + socket.id + "] has disconnected!");
		});
		
//		// send to current request socket client
//		socket.emit('message', "this is a test");
//
//		// sending to all clients, include sender
//		io.sockets.emit('message', "this is a test");
//
//		// sending to all clients except sender
//		socket.broadcast.emit('message', "this is a test");
//
//		// sending to all clients in 'game' room(channel) except sender
//		socket.broadcast.to('game').emit('message', 'nice game');
//
//		// sending to all clients in 'game' room(channel), include sender
//		io.sockets.in('game').emit('message', 'cool game');
//
//		// sending to individual socketid
//		io.sockets.socket(socketid).emit('message', 'for your eyes only');
	});
}

// sending to all clients
function newImage(imgUrl) {
	io.sockets.emit("newImage", imgUrl);
}

exports.start = start;
exports.newImage = newImage;