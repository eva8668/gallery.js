var server = require("./server");
var router = require("./router");
var requestHandlers = require("./requestHandlers");
var notificationHandler = require("./notificationHandler");

var handle = {};
handle["/start"] = requestHandlers.start;
handle["/upload"] = requestHandlers.upload;
handle["/*.*"] = requestHandlers.staticFile;
handle["/getImages"] = requestHandlers.getImages;

server.start(router.route, handle, notificationHandler);