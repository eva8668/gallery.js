var http = require("http");
var url = require("url");

function start(route, handle, notificationHandler) {
	function onRequest(request, response) {
		var pathname = url.parse(request.url).pathname;
		console.log("Request for " + pathname + " received.");
		route(handle, pathname, request, response, notificationHandler);
	}

	var httpServer = http.createServer(onRequest).listen(8888);
	notificationHandler.start(httpServer);
	console.log("Server has started.");
}

exports.start = start;