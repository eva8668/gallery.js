function route(handle, pathname, request, response, notificationHandler) {
	console.log("About to route a request for " + pathname);
	if (typeof handle[pathname] == "function") {
		handle[pathname](request, response, notificationHandler);
	} else {
		handle["/*.*"](request, response);
	}
}

exports.route = route;