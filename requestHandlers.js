var querystring = require("querystring"),
    fs = require("fs"),
    mime = require("mime"),
    formidable = require("formidable"),
    url = require("url");

function start(request, response) {
	console.log("Request handler 'start' was called.");
	
	var body = '<html>' +
	  '<head>' +
	  '<meta http-euqiv="Content-Type" content="text/html; ' +
	  'charset=UTF-8">' +
	  '</head>' +
	  '<body>' +
	  '<form action="/upload" enctype="multipart/form-data" method="post">' +
	  '<input type="file" name="upload" multiple="multiple" />' +
	  '<input type="submit" value="Upload file" />' +
	  '</form>' +
	  '</body>' +
	  '</html>';
	
	response.writeHead(200, {"Content-Type": "text/html"});
	response.write(body);
	response.end();
}

function upload(request, response, notificationHandler) {
	console.log("Request handler 'upload' was called.");
	
	var form = new formidable.IncomingForm();
	form.uploadDir="./tmp";
	console.log("about to parse");
	form.parse(request, function(error, fields, files) {
		console.log("parsing done");
		var fileTo = new Date().getTime() + "_" + files.upload.name;
		fs.renameSync(files.upload.path, "./webRoot/assets/" + fileTo);
		//response.writeHead(301, {'Location':'/'});
		response.end();
		
		// Send notification to all clients
		notificationHandler.newImage("/assets/" + fileTo);
	});
}

function staticFile(request, response) {
	var pathname = url.parse(request.url).pathname;
	
	// Set up default homepage
	if (pathname == "/") {
		pathname = "/index.html";
	}
	
	fs.readFile(__dirname + "/webRoot" + pathname, function(err, data) {
		if (err) {
			console.log("No web page found for " + pathname);
			response.writeHead(404, {"Content-type": "text/plain"});
			response.write("404 Not found");
			return response.end();
		}
		// var type = mime.lookup(__dirname + "/webRoot" + pathname);
		// console.log("GET File " + pathname + ", type: " + type);
		response.writeHead(200 /*, {"Content-type": type} */);
		response.end(data);
	});
}

function getImages(request, response) {
	var fileUrl = new Array();
	fs.readdir(__dirname + "/webRoot/assets/", function(err, files) {
		// Adjust filename to relative URL path
		for (var i in files) {
			fileUrl.push("/assets/" + files[i]);
		}
		// Set up response
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.write(JSON.stringify(fileUrl));
		response.end();
	});
}

exports.start = start;
exports.upload = upload;
exports.staticFile = staticFile;
exports.getImages = getImages;
